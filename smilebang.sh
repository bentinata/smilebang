#!/bin/sh

i=${1:-3}
while [ $i -gt 0 ]; do
  j=$i
  while [ $j -gt 0 ]; do
    echo -n "Smile!";
    j=$(($j - 1));
  done
  echo;
  i=$(($i - 1));
done
