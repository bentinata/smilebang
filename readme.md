![Shit](./smilebang.png)

> How do I write a program that produces the following output?  
> Smile!Smile!Smile!  
> Smile!Smile!  
> Smile!  

That is, the piece of the question asked by some anonymous person from the internet.
"Lazy kid" you may say.
But that question gathers some of the interesting programming language.

Here's the original Quora thread.
https://www.quora.com/Homework-Question-How-do-I-write-a-program-that-produces-the-following-output-1

# Name
Smilebang is "smile" and "bang".
Just like "hashbang" is hash and bang.

# Approach
Since I love pixelart, I use Piet.
An esoteric language based on pixel and color.
See it [here](http://www.dangermouse.net/esoteric/piet.html)

Piet need to be passed an parameter.
Since it doesn't support default parameter.

Common interpreter for it is `npiet`.
To execute it, type:
```
echo 3 | npiet -q smilebang.png
```
in your shell.

For different output, pass different number to echo.

For example:
```
echo 5 | npiet -q smilebang.png
```
will yield:
```
Smile!Smile!Smile!Smile!Smile!
Smile!Smile!Smile!Smile!
Smile!Smile!Smile!
Smile!Smile!
Smile!
```

# Character Codes
Char  Bin       Dec   Hex
S     01010011  83    53
m     01101101  109   6d
i     01101001  105   69
l     01101100  108   6c
e     01100101  101   65
!     00100001  33    21
\n    00001010  10    a

# Trace
Now `npiet` got trace! Whooo!

![Trace](./npiet-trace.png)
